
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="../greet/src/assests/greet-logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">GREET API TASK</h3>

  <p align="center">
    SPA (single-page application) consuming given API endpoint
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

The project is a SPA task, which explores a given API endpoint. It presents an data in the format of carts which, individually contains distinctive information (image, name, description, category, 'Add to Cart' button and price). When clicked on 'Add to Cart' it redirects the user to unique URL of the given cart. The application has an option to filter the loaded data by category, where the category is dynamically populated, based on the loaded results.There is an option, to clear the filter and show all of data. Also, there is a option to sort the result by name (A-Z), price (ascending, descending), and return it in original order.  

Use the `README.md` to get started.

### Built With

Technology used to create the GREET SPA:

* [![Javascript]][Javascript-url]
* [![HTML][HTML]][HTML-url]
* [![CSS][CSS]][CSS-url]
* [![React][React.js]][React-url]
* [![Bootstrap][Bootstrap.com]][Bootstrap-url]

<!-- GETTING STARTED -->
## Getting Started

Below are the main instruction of how to get, install, run and use the app locally

### Project structure


* 
  ```sh
  greet
  |--- src
  |    |--- assets
  |    |     |- greet-logo.png
  |    |--- components
  |    |     |--- AddToCartBtn
  |    |     |   |--- AddToCartBtn.jsx
  |    |     |--- Main
  |    |     |   |--- Main.jsx
  |    |     |--- UserCart
  |    |     |   |--- UserCart.jsx
  |    |--- constants
  |    |     |--- all-categories.js
  |    |     |--- default-sorting-option.js
  |    |     |--- greet-api.js
  |    |     |--- sorting-options.js
  |    |--- App.css
  |    |--- App.jsx
  |    |--- index.css
  |    |--- main.jsx
  |--- .eslintrc.cjs
  |--- .gitignore
  |--- .index.html
  |--- package.json
  |--- package-lock.json
  |--- README.md
  |--- vite.config
  ```

### Installation

_Below are the steps required to get, install, run and use the project._

1. Clone the repo (_Clone with HHTPS_)
   ```sh
   git clone https://gitlab.com/kristiyanT/greet.git
   ```
2. Install NPM packages
   ```sh
   npm install || npm i
   ```
3. Usage
   ```sh
   npm run dev
   ```
   

<!-- CONTACT -->
## Contact

Kristiyan Todorov - [@LinkedIn](https://www.linkedin.com/in/kristiyan-todorov-34a550292/) - yonkov.kristiyan@gmail.com

Project Link: [Greet SPA Task](https://gitlab.com/kristiyanT/greet)

<!-- MARKDOWN LINKS & IMAGES -->
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[HTML]: https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white
[HTML-url]: https://www.w3.org/
[CSS]: https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white
[CSS-url]: https://www.w3.org/
[Javascript]: https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E
[Javascript-url]: https://en.wikipedia.org/wiki/JavaScript
