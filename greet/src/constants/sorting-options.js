export const SORTING_OPTIONS = [{
    id: 'INITIAL_ORDER',
    label: 'Първоначален ред'
}, {
    id: 'NAME',
    label: 'Име'
}, {
    id: 'PRICE_ASC',
    label: 'Цена възходящи'
}, {
    id: 'PRICE_DESC',
    label: 'Цена низходящи'
}]