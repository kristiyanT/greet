import Button from 'react-bootstrap/Button';

export const AddToCartBtn = (props) => {
    const {
        onClick,
        size = 'sm',
        label = 'Add to cart',
        variant = 'outline-primary'
    } = props;

    return (
        <Button
            onClick={onClick}
            variant={variant}
            size={size}
        >
            {label}
        </Button>
    )
}
