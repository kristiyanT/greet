import { Card, Col, Container, Row, } from "react-bootstrap";
import { AddToCartBtn } from '../AddToCartBtn/AddToCartBtn';
import Badge from 'react-bootstrap/Badge';
import styled from "styled-components";




export const UserCart = ({ data, onScroll, listInnerRef }) => {

    const goToExternalLink = (id) => {
        window.open(`https://greet.bg/${id}`, "_blank")
    }

    return (
        <>
            <ContainerDiv
                onScroll={onScroll}
                ref={listInnerRef}
            >
                <Row>
                    {data.map((el) => {
                        return (
                            <Col
                                md="auto"
                                key={el.id}
                                className="mb-4">
                                <Card style={{ width: '18rem' }}>
                                    <Card.Img variant="top" src={el.images[0].src} />
                                    <Card.Body>
                                        <Card.Title>
                                            <CardTitleDiv
                                                dangerouslySetInnerHTML={{ __html: el.name }}
                                            />
                                        </Card.Title>
                                        <CardDescriptionDiv
                                            dangerouslySetInnerHTML={{ __html: el.short_description }}
                                        />
                                        <div>
                                            {`${el.prices.regular_price.substring(0,2)}${el.prices.currency_suffix}`}
                                        </div>
                                        <AddToCardDiv>
                                            <AddToCartBtn onClick={() => {
                                                goToExternalLink(el.add_to_cart.url)
                                            }} />
                                        </AddToCardDiv>
                                        <CardSubtitle>
                                            {el.categories.map((category) => (
                                                <BadgeWithMargin
                                                    bg="warning"
                                                    text="dark"
                                                    key={category.id}
                                                    pill
                                                >
                                                    {category.name}
                                                </BadgeWithMargin>
                                            ))}
                                        </CardSubtitle>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            </ContainerDiv >
        </>
    );
}

const ContainerDiv = styled.div`
  height: 100vh;
  overflow: auto;
  justify-content: space-around
`;

const CardTitleDiv = styled.div`
height: 50px;
display: flex;
align-items: center;
justify-content: center;
`;

const CardDescriptionDiv = styled.div`
height: 150px;
overflow: hidden;
align-items: center;
`;

const AddToCardDiv = styled.div`
margin-top: 10px;
`;

const CardSubtitle = styled.div`
margin-top: 10px;
min-height: 120px;
`;

const BadgeWithMargin = styled(Badge)`
  margin: 0.2rem;
`;