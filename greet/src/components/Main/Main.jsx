import { useState, useEffect, useRef, useMemo } from 'react'
import { styled } from "styled-components";
import { Row } from 'react-bootstrap';
import { Dropdown, DropdownButton, Container } from "react-bootstrap";

import { UserCart } from '../UserCart/UserCart'
import { GREET_API } from '../../constants/greet-api'
import { DEFAULT_SORTING_OPTION_ID } from '../../constants/default-sorting-options';
import { ALL_CATEGORIES } from '../../constants/all-categories';
import { SORTING_OPTIONS } from '../../constants/sorting-options';

export const Main = () => {
    const listInnerRef = useRef()
    const [currentPage, setCurrentPage] = useState(1)
    const [previousPage, setPreviousPage] = useState(0)
    const [lastList, setLastList] = useState(false)
    const [userData, setUserData] = useState([])
    const [sortedRes, setSortedRes] = useState([])
    const [selectedCategory, setSelectedCategory] = useState(ALL_CATEGORIES)
    const [sortOptions, setSorOptions] = useState(DEFAULT_SORTING_OPTION_ID)

    const availableCategories = useMemo(() => {
        const uniqueCategories = userData.reduce((acc, item) => {
            item.categories.forEach((category) => {
                if (acc.indexOf(category.name) === -1) {
                    acc.push(category.name)
                }
            })
            return acc;
        }, []);
        return [ALL_CATEGORIES].concat(uniqueCategories)
    }, [
        userData
    ])

    useEffect(() => {

        try {
            const fetchData = async () => {
                const res = await fetch(`${GREET_API}${currentPage}`)
                const resToJson = await res.json()

                if (!resToJson.length) {
                    setLastList(true);
                    return;
                }


                setPreviousPage(currentPage)
                setUserData([...userData, ...resToJson])
            }

            if (!lastList && previousPage !== currentPage) {
                fetchData();
            }
        } catch (e) {
            console.error(`Error: ${e}`)
        }

    }, [currentPage, lastList, previousPage, userData])

    useEffect(() => {
        const data = [...userData]

        switch (sortOptions) {
            case "NAME": {
                data.sort((a, b) => (a.name > b.name ? 1 : -1));
                break;
            }
            case "PRICE_ASC": {
                data.sort((a, b) => (a.prices.price > b.prices.price ? 1 : -1))
                break;
            }
            case "PRICE_DESC": {
                data.sort((a, b) => (a.prices.price > b.prices.price ? -1 : 1))
                break;
            }
        }

        setSortedRes(data)

    }, [sortOptions, userData])

    const onScroll = () => {
        if (listInnerRef.current) {
            const { scrollTop, scrollHeight, clientHeight } = listInnerRef.current;
            if (scrollTop + clientHeight === scrollHeight) {
                setCurrentPage(currentPage + 1)
            }
        }
    }

    const onCategoryChange = (category) => {
        setSelectedCategory(category)

    }

    const data = useMemo(() => {
        if (selectedCategory === ALL_CATEGORIES) {
            return sortedRes
        }

        return sortedRes.filter((person) => {
            return person.categories.some(((category) => category.name === selectedCategory))
        })
    }, [selectedCategory, sortedRes])


    const handleSortingChange = (newSortingOption) => {
        setSorOptions(newSortingOption);
    }

    return (
        <>
            <Container>
                <BtnContainer>
                    <StyledDropdownButton
                        disabled={userData.length === 0}
                        className="mb-2" title={selectedCategory || ALL_CATEGORIES} onSelect={onCategoryChange}>
                        {availableCategories.map(category => (
                            <Dropdown.Item disabled={category === selectedCategory} eventKey={category} key={category}>
                                {category}
                            </Dropdown.Item>
                        ))}
                    </StyledDropdownButton>
                    <StyledDropdownButton
                        disabled={userData.length === 0}
                        className="mb-2" title={SORTING_OPTIONS.find(option => option.id === sortOptions).label}
                        onSelect={handleSortingChange} selected={sortOptions}>
                        {SORTING_OPTIONS.map((option) => {
                            return (
                                <Dropdown.Item
                                    key={option.id}
                                    disabled={option.id === sortOptions}
                                    eventKey={option.id}
                                >
                                    {option.label}
                                </Dropdown.Item>
                            )
                        })}
                    </StyledDropdownButton>
                </BtnContainer>
                <FlexBox>
                    <PostContainer>
                        <PostRow>
                            <UserCart
                                data={data}
                                onScroll={onScroll}
                                listInnerRef={listInnerRef}
                            />
                        </PostRow>
                    </PostContainer>
                </FlexBox>
            </Container>
        </>
    )
}

export default Main

const PostContainer = styled.div``;

const BtnContainer = styled.div`
  padding-top: 10px;
  display: flex;
  justify-content: center
`;

const StyledDropdownButton = styled(DropdownButton)`
  margin: 10px;
`;

const FlexBox = styled.div`
  display: flex;
  justify-content: start;
`;

const PostRow = styled(Row)`
  justify-content: space-around;
`;